package eelects.collection.dao;

import eelects.collection.entities.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VoteRepository extends JpaRepository<Vote,Long> {
    public List<Vote> getByCandidatIdAndStudentId(Long id, Long id2);
}
