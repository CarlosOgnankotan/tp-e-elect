package eelects.collection.service;

import eelects.collection.dao.StudentRepository;
import eelects.collection.entities.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;
    /**
     * Ce service récupère la liste des étudiants
     */

    public List<Student> getStudents() {
        return studentRepository.findAll();
    }
/**
 * Ce service ajoute un nouveau étudiant
 */
    public Student addStudent(Student student) {
        return studentRepository.save(student);
    }

    //@Override
    public void updateStudent(Long id, Student student) {
        Student s = this.studentRepository.findById(id).get();

        s.setFirstName(student.getFirstName());
        s.setLastName(student.getLastName());
        s.setAddress(student.getAddress());
        s.setDateN(student.getDateN());
        s.setEmail(student.getEmail());
        s.setPassword(student.getPassword());
        this.studentRepository.save(s);

    }
    public Student getStudentById(Long id) {
        return this.studentRepository.findById(id).get();
    }

    //@Override
    public void deleteOne(Long id) {
        this.studentRepository.deleteById(id);
    }

}
