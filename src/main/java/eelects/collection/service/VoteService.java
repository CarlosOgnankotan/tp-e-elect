package eelects.collection.service;

import eelects.collection.dao.CandidatRepository;
import eelects.collection.dao.StudentRepository;
import eelects.collection.dao.VoteRepository;
import eelects.collection.entities.Candidat;
import eelects.collection.entities.Student;
import eelects.collection.entities.Vote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class VoteService {
    @Autowired
    private VoteRepository voteRepository;
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private CandidatRepository candidatRepository;

    /**
     * Ce service ajoute un vote
     */
    public boolean addVote(Long studentId, Long candidaId ) {

        Student etude = studentRepository.findById(studentId).orElse(null);
        Candidat candi = candidatRepository.findById(candidaId).orElse(null);
        // Vérifier si l'etudiant a déjà voté (si son ID existe dans la base de donnée dans la table vote)
        try {
            List<Vote> votelist = voteRepository.getByCandidatIdAndStudentId(etude.getId(), candi.getId());
            if(votelist.size() > 0) {
                return false;
            }
            //enregistrer le vote
            Vote vote = new Vote();
            Date d=new Date();
            vote.setDateVote(d);
            vote.setStudent(etude);
            vote.setCandidat(candi);
            voteRepository.save(vote);

            return  true;

        }catch (Exception ex){
            System.out.println(ex);
            return false;
        }

    }





}
