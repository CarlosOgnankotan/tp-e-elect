package eelects.collection.service;

import eelects.collection.dao.CandidatRepository;
import eelects.collection.entities.Candidat;
import eelects.collection.entities.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CandidatService {
    @Autowired
    private CandidatRepository candidatRepository;
    /**
     * Ce service récupère la liste des étudiants
     */

    public List<Candidat> getCandidats() {
        return candidatRepository.findAll();
    }
    /**
     * Ce service ajoute un nouveau étudiant
     */
    public Candidat addCandidat(Candidat candidat) {
        return candidatRepository.save(candidat);

    }

    //@Override
    public void updateCandidat(Long id, Candidat candidat) {
        Candidat c = this.candidatRepository.findById(id).get();

        c.setFirstName(candidat.getFirstName());
        c.setLastName(candidat.getLastName());

        this.candidatRepository.save(c);

    }
    public Candidat getCandidatById(Long id) {
        return this.candidatRepository.findById(id).get();
    }

    //@Override
    public void deleteCandidat(Long id) {
        this.candidatRepository.deleteById(id);
    }
}
