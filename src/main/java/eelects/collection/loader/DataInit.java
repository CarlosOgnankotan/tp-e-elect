package eelects.collection.loader;


import eelects.collection.dao.StudentRepository;
import eelects.collection.entities.Student;
import org.springframework.boot.CommandLineRunner;

public class DataInit implements CommandLineRunner {
    public final StudentRepository studentRepository;

    public DataInit(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }


    @Override
    public void run(String... args) throws Exception {
        loadStudents();
    }

    private void loadStudents() {
        if (this.studentRepository.count() == 0) {
            Student student = new Student(
                    "Laoudi",
                    "Youcef",
                    "Paris",
                    "02/09/1998",
                    false,
                    "youcef@gmail.com",
                    ""
            );
            this.studentRepository.save(student);

            System.out.println("--> insert finished");
        }
    }
}
