package eelects.collection.controllers;

import eelects.collection.entities.Student;
import eelects.collection.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/student")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping
    public List<Student> showAll(){
        return studentService.getStudents();
    }

    @PostMapping
    public Student postStudent(Student student){
        Student student1 = new Student();
        try {
            if (student != null) {
                student1 = student;
                return studentService.addStudent(student1);
            } else
                return null;
            }
            catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }
    }

    @PutMapping({"/{id}"})
    public ResponseEntity<Student> updateStudent(@PathVariable("id") Long id, Student student) {
        Student student1 = new Student();
        if (student != null) {
            student1 = student;
            this.studentService.updateStudent(id, student1);
            return new ResponseEntity<>(this.studentService.getStudentById(id), HttpStatus.OK);
        } else
            return null;


    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<Student> deleteStudent(@PathVariable("id") Long id) {
        this.studentService.deleteOne(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
