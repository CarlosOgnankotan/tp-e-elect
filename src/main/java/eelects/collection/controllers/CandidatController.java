package eelects.collection.controllers;

import eelects.collection.entities.Candidat;

import eelects.collection.service.CandidatService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/candidat")
public class CandidatController {
    @Autowired
    private CandidatService candidatService;

    @GetMapping
    public List<Candidat> showAll(){
        return candidatService.getCandidats();
    }

    @PostMapping
    public Candidat postCandidat(@RequestBody Candidat candidat){
        try {
            if (candidat != null) {
                return candidatService.addCandidat(candidat);
            } else
                return null;
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }
    }
    @PutMapping({"/{id}"})
    public ResponseEntity<Candidat> updateCandidat(@PathVariable("id") Long id, @RequestBody Candidat candidat) {
        this.candidatService.updateCandidat(id, candidat);
        return new ResponseEntity<>(this.candidatService.getCandidatById(id), HttpStatus.OK);
    }

    @DeleteMapping({"/{id}"})
    public ResponseEntity<Candidat> deleteCandidat(@PathVariable("id") Long id) {
        this.candidatService.deleteCandidat(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
